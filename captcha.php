<?php
/*-------------------------------------------------------------------------

     Copyright 2008 Jure Sah

     This file is part of Principles and Classes database.

     Principles and Classes database is free software: you can redistribute 
     it and/or modify it under the terms of the GNU General Public License 
     as published by the Free Software Foundation, either version 3 of the 
     License, or (at your option) any later version.

     Please refer to the README file for additional information.

-------------------------------------------------------------------------*/
 Header('Cache-Control: no-cache');
 Header('Pragma: no-cache');
 header("Content-type: image/png");

 $content= '<?xml version="1.0" encoding="UTF-8" standalone="no"?><svg><text font-family="Pursia" x="0" y="20" style="font-size: 20;">'. htmlentities($_SERVER['REMOTE_ADDR'],ENT_QUOTES,'UTF-8').'</text></svg>';

 passthru("echo ".escapeshellarg($content)."| rsvg-convert /dev/stdin")
?>
