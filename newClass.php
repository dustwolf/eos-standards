<?php
/*-------------------------------------------------------------------------

     Copyright 2008 Jure Sah

     This file is part of Principles and Classes database.

     Principles and Classes database is free software: you can redistribute 
     it and/or modify it under the terms of the GNU General Public License 
     as published by the Free Software Foundation, either version 3 of the 
     License, or (at your option) any later version.

     Please refer to the README file for additional information.

-------------------------------------------------------------------------*/
if($_POST && $_POST["captcha"]==$_SERVER['REMOTE_ADDR'] && strlen($_POST["definition"])>0) {
include 'credentials.php';
mysql_connect($hostname, $username, $password);
mysql_set_charset('utf8');

 mysql_select_db("netPrinciples");

 $now=time();
 $ID=date("Y-m-d H:i:s",$now);
 $friendlyName=mysql_real_escape_string(htmlspecialchars($_POST["friendlyName"],ENT_QUOTES,'UTF-8'));
 $Definition=mysql_real_escape_string(htmlspecialchars($_POST["definition"],ENT_QUOTES,'UTF-8'));
 $query="INSERT INTO netClasses VALUES('".$ID."','".$friendlyName."','".$Definition."','')"; 
 $result=mysql_query($query) or die(mysql_error());
 mysql_select_db("netPrinciples");

 $myID="2-".date("Ymd-His",$now);
 header('Location: http://standards.ctrl-alt-del.si/index.php/'.$myID);

mysql_close();
} else {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta content="text/html; charset=UTF-8" http-equiv="content-type" />
  <title>Principles and Classes - New Class</title>
</head>
<body>

<table style="text-align: left; width: 100%;" border="0"
cellpadding="20" cellspacing="10">
<tbody>
<tr>
<td style="vertical-align: top; 
         width: 120px; 
         background-color: rgb(71, 117, 255);">
<?php include "menu.php"; ?>
</td>
<td style="vertical-align: top;">

<h1>Principles and Classes - New Class</h1>

Enter a new class here. Fill in the required principle IDs in a comma seperated (or other readable) list and optionally a friendly name.<br/>
<br/>
The ID will be automatically generated. When refering to principles or other classes, just write the ID, the link and name will be added automatically. We suggest you open a new tab and <a href="search.php">search</a> trough the principles to include.<br/>
<br/>
When you have submitted a valid entry, you will be automatically redirected to the new entry.<br/>
<br/>
<form method="post" action="<?php echo $PHP_SELF;?>">
Friendly name:<br/>
<input name="friendlyName" type="text" size="50" value="<?php echo $_POST["friendlyName"];?>"/><br/><br/>
Requires:<br/>
<textarea rows="10" cols="60" name="definition"><?php echo $_POST["definition"];?></textarea><br/><br/>
Also enter what you see here:<br/>
<img src="http://standards.ctrl-alt-del.si/captcha.php"><br/>
<input name="captcha" type="text" value="<?php echo $_POST["captcha"];?>" /><br/><br/>
<input type="submit" value="Publish">

</form>


      </td>
    </tr>
  </tbody>
</table>

</body>
</html>
<?php } ?>
