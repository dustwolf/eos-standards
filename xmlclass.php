<?php
/*-------------------------------------------------------------------------

     Copyright 2008 Jure Sah

     This file is part of Principles and Classes database.

     Principles and Classes database is free software: you can redistribute 
     it and/or modify it under the terms of the GNU General Public License 
     as published by the Free Software Foundation, either version 3 of the 
     License, or (at your option) any later version.

     Please refer to the README file for additional information.

-------------------------------------------------------------------------*/
header("Content-type: text/xml;charset=utf-8");
$src=substr(rawurldecode($_SERVER["PATH_INFO"]),1);
if(substr($src,1,1)=='-' && substr($src,10,1)=='-' && strlen($src)==17) {
 $ID=substr($src,2,4)."-".substr($src,6,2)."-".substr($src,8,2)." "
    .substr($src,11,2).":".substr($src,13,2).":".substr($src,15,2);
 $ID=mysql_real_escape_string($ID);
  
 include 'credentials.php';
 include 'linkifyxml.php';
 
 mysql_connect($hostname, $username, $password);
 mysql_set_charset('utf8');
 mysql_select_db("netPrinciples");
 
 echo '<?xml version="1.0" encoding="UTF-8" standalone="no"?>';
 
 if(substr($src,0,1)=="1") {
  echo '<principle ID="'.htmlentities($src).'">';
 
  $tabela=mysql_query("SELECT * FROM netPrinciples WHERE ID='".$ID."'");
  $friendlyName=stripslashes(mysql_result($tabela,0,"friendlyName"));
  $Definition=stripslashes(mysql_result($tabela,0,"Definition"));
 
  echo '<friendlyName>'.$friendlyName.'</friendlyName>';
 
  echo '<definition>'.$Definition.'</definition>';
 
  $vrstica++;
 }
 
 if(substr($src,0,1)=="2") {
  echo '<class ID="'.htmlentities($src).'">';
  $tabela=mysql_query("SELECT * FROM netClasses WHERE ID='".$ID."'");
  $friendlyName=stripslashes(mysql_result($tabela,0,"friendlyName"));
  $D=stripslashes(mysql_result($tabela,0,"Requires"));
  echo '<friendlyName>'.$friendlyName.'</friendlyName>';
  echo '<requires text="'.$D.'">';
  $l=strlen($D);
  $p=0;
  while($p<$l) {
   if(substr($D,$p,2)=="1-") {
    $gID=substr($D,$p,17);
    echo linkify($gID,1).", ";
    $p=$p+16;
   } 
   if(substr($D,$p,2)=="2-") {
    $gID=substr($D,$p,17);
    echo linkify($gID,1).", ";    
    $p=$p+16;
   }
   $p++;
  }
  echo '</requires>';
 }
 
 if(substr($src,0,1)=="1") echo '</principle>';
 if(substr($src,0,1)=="2") echo '</class>';
 mysql_close();
}
?>
