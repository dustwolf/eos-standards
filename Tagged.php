<?php
/*-------------------------------------------------------------------------

     Copyright 2008 Jure Sah

     This file is part of Principles and Classes database.

     Principles and Classes database is free software: you can redistribute 
     it and/or modify it under the terms of the GNU General Public License 
     as published by the Free Software Foundation, either version 3 of the 
     License, or (at your option) any later version.

     Please refer to the README file for additional information.

-------------------------------------------------------------------------*/
function Tagged($ID) {
 $partOf=mysql_query("SELECT ID,Owner FROM netTags WHERE Signature LIKE '%".$ID."%'");
 $pvrstica=0;
 $pvrstice=mysql_numrows($partOf);
 if($pvrstice>0) echo '<br/>Tagged by: ';
 while($pvrstica<$pvrstice) {
  $gID=stripslashes(mysql_result($partOf,$pvrstica,"ID"));
  $gID=str_replace("-","",$gID);
  $gID=str_replace(":","",$gID);
  $gID=str_replace(" ","-",$gID);
  $gID="3-".$gID;
  $pOwner=stripslashes(mysql_result($partOf,$pvrstica,"Owner"));

  echo '<a href="http://standards.ctrl-alt-del.si/index.php/'.$gID.'">'
       .$pOwner.'</a>';
  if($pvrstica<$pvrstice-1) echo ', ';
  $pvrstica++;
 }
}
?>
