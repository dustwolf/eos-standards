<?php
/*-------------------------------------------------------------------------

     Copyright 2008 Jure Sah

     This file is part of Principles and Classes database.

     Principles and Classes database is free software: you can redistribute 
     it and/or modify it under the terms of the GNU General Public License 
     as published by the Free Software Foundation, either version 3 of the 
     License, or (at your option) any later version.

     Please refer to the README file for additional information.

-------------------------------------------------------------------------*/
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta content="text/html; charset=UTF-8" http-equiv="content-type" />
  <title>Principles and Classes - About</title>
</head>
<body>

<table style="text-align: left; width: 100%;" border="0"
cellpadding="20" cellspacing="10">
<tbody>
<tr>
<td style="vertical-align: top; 
	 width: 120px; 
	 background-color: rgb(71, 117, 255);">
<?php include "menu.php"; ?>
</td>
<td style="vertical-align: top; text-align: justify;">

<h1>Principles and Classes - About</h1>

Principles and Classes represent a framework for the self-improvement of society.<br/>
<br/>
Principles are the goals and eventually the rules we set for ourselves. In this database you enter them as guidelines with a definition, what should a person or organization do to be in spirit with the principle. You give it a name so people know what to call it.<br/>
<br/>
Classes are the labels we put on people. Labels which help us know what kinds of goals people or organizations follow in life. Here the meaning is not at all negative, here a Class is defined by a set of Principles it requires. Do not include explanations in the defenition of Classes, just list the required Principles by ID.<br/>
<br/>
Tags are signatures, indicating that we approve of a Principle or Class. You sign Tags with a PGP signature to prove authorship. Tags can be canceled by the author if (s)he no longer aproves with the Principle or Class in question.<br/>
<br/>
The database gives each submission an ID, to help uniquely identify it and thus provide a standard way of refering to it. In order to avoid situations where strange new rules are being smuggled into existing Principles or Classes, that people follow, there is no way to alter a submission once it has been posted, so please think before posting. Posting is open to all, in order to get as many good ideas as we possibly can.<br/> 
<br/>
But please, no test posts.<br/>
<br/>
The database provides an output you can integrate into your website in the form of a RSS feed containing all entries Tagged by a specific person. The URL of this feed is http://standards.ctrl-alt-del.si/rsstagged.php/ followed by the 40-character hexadecimal fingerprint of the person's PGP signature.<br/>
<br/>
See <a href="http://standards.ctrl-alt-del.si/index.php/2-20081011-235457">2-20081011-235457</a> for some goals to look towards when writing new Principles and / or Classes.<br/>
<br/>
<a href="http://pritisni.ctrl-alt-del.si/NET/NPO%20cooperative%20in%20prototechnate.html">There is an article currently written, detailing the idea and it's mechanism of action.</a><br/>
<br/>
This project is open source, it is based on Apache, PHP, MySQL, PEAR's Crypt_GPG and librsvg2-bin. The source code is available for download <a href="http://bitbucket.org/dustwolf/principles-and-classes-database/">here</a>.
      </td>
    </tr>
  </tbody>
</table>

</body>
</html>

