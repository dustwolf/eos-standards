<?php
/*-------------------------------------------------------------------------

     Copyright 2008 Jure Sah

     This file is part of Principles and Classes database.

     Principles and Classes database is free software: you can redistribute 
     it and/or modify it under the terms of the GNU General Public License 
     as published by the Free Software Foundation, either version 3 of the 
     License, or (at your option) any later version.

     Please refer to the README file for additional information.

-------------------------------------------------------------------------*/
function linkify($text, $label) {
$l=strlen($text);
$p=0;
 while($p<$l) {
  if(substr($text,$p,2)=="1-" || substr($text,$p,2)=="2-" || substr($text,$p,2)=="3-") {
   $src=substr($text,$p,17);
   if(substr($src,1,1)=='-' && substr($src,10,1)=='-') {
    if($label==1) {
      $myID=substr($src,2,4)."-".substr($src,6,2)."-".substr($src,8,2)." "
         .substr($src,11,2).":".substr($src,13,2).":".substr($src,15,2);
      $myID=mysql_real_escape_string($myID);
     $labelText="";
     if(substr($text,$p,2)=="1-") {
      $labelFetch=mysql_query("SELECT ID,friendlyName FROM netPrinciples WHERE ID='".$myID."'");
      if(mysql_numrows($labelFetch)>0) $labelText=stripslashes(mysql_result($labelFetch,0,"friendlyName"));
     } elseif(substr($text,$p,2)=="2-") {
      $labelFetch=mysql_query("SELECT ID,friendlyname FROM netClasses WHERE ID='".$myID."'");
      if(mysql_numrows($labelFetch)>0) $labelText=stripslashes(mysql_result($labelFetch,0,"friendlyName"));
     } elseif(substr($text,$p,2)=="3-") {
      $labelFetch=mysql_query("SELECT ID,Owner FROM netTags WHERE ID='".$myID."'");
      if(mysql_numrows($labelFetch)>0) $labelText=stripslashes(mysql_result($labelFetch,0,"Owner"));
     }
     if($labelText=="") {
      //No label found
      $out=$out.'[http://standards.ctrl-alt-del.si/index.php/'.$src.' '.$src.']';
     } else {
      //label
      $out=$out.'[http://standards.ctrl-alt-del.si/index.php/'.$src.' '.$labelText.']';
     }
    } else {
     //opted not to do labels
     $out=$out.'[href="http://standards.ctrl-alt-del.si/index.php/'.$src.' '.$src.']';
    }
    $p=$p+16;
   } else { 
    $out=$out.substr($text,$p,1);
   }
  } else {
   $out=$out.substr($text,$p,1);
  }
 $p++;
 }
return $out;
}
?>
