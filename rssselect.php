<?php
/*-------------------------------------------------------------------------

     Copyright 2008 Jure Sah

     This file is part of Principles and Classes database.

     Principles and Classes database is free software: you can redistribute 
     it and/or modify it under the terms of the GNU General Public License 
     as published by the Free Software Foundation, either version 3 of the 
     License, or (at your option) any later version.

     Please refer to the README file for additional information.

-------------------------------------------------------------------------*/
header("Content-type: application/rss+xml;charset=utf-8");

 include 'credentials.php';

 mysql_connect($hostname, $username, $password);
 mysql_set_charset('utf8');
 mysql_select_db("netPrinciples");

$line=htmlentities(substr(rawurldecode($_SERVER["PATH_INFO"]),1),ENT_QUOTES,'UTF-8');
$l=strlen($line) - 16; $p=0;
echo '<?xml version="1.0" encoding="UTF-8" standalone="no"?>';
?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
 <channel>
  <title>Principles and Classes selective feed</title>
  <link>
   http://standards.ctrl-alt-del.si/rssselect.php<?php echo $_SERVER["PATH_INFO"]; ?>
  </link>
  <atom:link href="http://standards.ctrl-alt-del.si/rssselect.php<?php echo $_SERVER["PATH_INFO"]; ?>" rel="self" type="application/rss+xml" />
  <description>
   <?php echo $line; ?>
  </description>

<?php
while ($p<$l) {
$src=substr($line,$p,17);
if(substr($src,1,1)=='-' && substr($src,10,1)=='-') {
 $ID=substr($src,2,4)."-".substr($src,6,2)."-".substr($src,8,2)." "
    .substr($src,11,2).":".substr($src,13,2).":".substr($src,15,2);
 $ID=mysql_real_escape_string($ID);
 
 echo '<item>';
 echo '<pubDate>'.date(DATE_RFC822,mktime(substr($src,11,2),substr($src,13,2),substr($src,15,2),substr($src,6,2),substr($src,8,2),substr($src,2,4))).'</pubDate>';

 if(substr($src,0,1)=="1") {
  $tabela=mysql_query("SELECT * FROM netPrinciples WHERE ID='".$ID."'");
  if(mysql_numrows($tabela)>0) {
   $friendlyName=stripslashes(mysql_result($tabela,0,"friendlyName"));
   $Definition=stripslashes(mysql_result($tabela,0,"Definition"));
   echo '<title>'.htmlentities($src,ENT_QUOTES,'UTF-8').': '.$friendlyName.'</title>'; 
   echo '<description>'.$Definition.'</description>'; 
  } else {
   echo '<title>'.htmlentities($src,ENT_QUOTES,'UTF-8').'</title>';
  }
 }
 
 if(substr($src,0,1)=="2") {
  $tabela=mysql_query("SELECT * FROM netClasses WHERE ID='".$ID."'");
  if(mysql_numrows($tabela)>0) {
   $friendlyName=stripslashes(mysql_result($tabela,0,"friendlyName"));
   $D=stripslashes(mysql_result($tabela,0,"Requires"));
   echo '<title>'.htmlentities($src,ENT_QUOTES,'UTF-8').': '.$friendlyName.'</title>';
   echo '<description>'.$D.'</description>';
  } else {
   echo '<title>'.htmlentities($src,ENT_QUOTES,'UTF-8').'</title>';
  }
 }

 echo '<guid>http://standards.ctrl-alt-del.si/index.php/'.htmlentities($src,ENT_QUOTES,'UTF-8').'</guid>';
 echo '<link>http://standards.ctrl-alt-del.si/index.php/'.htmlentities($src,ENT_QUOTES,'UTF-8').'</link>';
 echo '</item>';
  $p=$p+17;
 } else {
  $p++;
 }
}
mysql_close();
?>
 </channel>
</rss>
