<?php
/*-------------------------------------------------------------------------

     Copyright 2008 Jure Sah

     This file is part of Principles and Classes database.

     Principles and Classes database is free software: you can redistribute 
     it and/or modify it under the terms of the GNU General Public License 
     as published by the Free Software Foundation, either version 3 of the 
     License, or (at your option) any later version.

     Please refer to the README file for additional information.

-------------------------------------------------------------------------*/
?><a style="color: yellow; font-weight: bold;" href="http://standards.ctrl-alt-del.si/about.php">About</a><br/>
<a style="color: yellow; font-weight: bold;" href="http://standards.ctrl-alt-del.si/newPrinciple.php">New Principle</a><br/>
<a style="color: yellow; font-weight: bold;" href="http://standards.ctrl-alt-del.si/newClass.php">New Class</a><br/>
<a style="color: yellow; font-weight: bold;" href="http://standards.ctrl-alt-del.si/newTag.php">New Tag</a><br/>
<a style="color: yellow; font-weight: bold;" href="http://standards.ctrl-alt-del.si/cancelTag.php">Cancel Tag</a><br/>
<a style="color: yellow; font-weight: bold;" href="http://standards.ctrl-alt-del.si/search.php">Search</a><br/>
