<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/1999/xhtml"
		version="1.0">

<xsl:output 	method="xml" encoding="UTF-8" indent="yes" 
		doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd" 
		doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN" />

<!--

     Copyright 2010 Jure Sah

     This file is part of Principles and Classes database.

     Principles and Classes database is free software: you can redistribute 
     it and/or modify it under the terms of the GNU General Public License 
     as published by the Free Software Foundation, either version 3 of the 
     License, or (at your option) any later version.

     Please refer to the README file for additional information.

-->

<xsl:template match="standards">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta content="text/html; charset=UTF-8" http-equiv="content-type" />
  <title>Principles and classes database - <xsl:value-of select="@file" /></title>
 </head>
 <body>
  <xsl:apply-templates />
 </body>
</html>
</xsl:template>

<xsl:template match="principle">
<table 	style="text-align: justify; width: 95%; left; margin-left: auto; margin-right: auto;"
	border="0" cellpadding="10" cellspacing="2">
 <tbody>
  <tr>
   <td style="color: white; background-color: rgb(130, 130, 30);">
    <a href="{ID}.html" style="color: white; text-decoration: none;"><xsl:value-of select="name" /><small> (<xsl:value-of select="ID" />)</small></a>
   </td>
  </tr>
  <xsl:if test="definition">
   <tr>
    <td><xsl:value-of select="definition" /><br />
    <xsl:for-each select="partOf">
     <xsl:if test="count(class)&gt;0">
      <br/>
      Part of:<br/>
      <xsl:apply-templates />
     </xsl:if>
    </xsl:for-each>
    <xsl:for-each select="tags">
     <xsl:if test="count(tag)">
      <br/>
      Approved by:<br/>
      <xsl:apply-templates />
     </xsl:if>
    </xsl:for-each>
    </td>
   </tr>
  </xsl:if>
 </tbody>
</table>
</xsl:template>

<xsl:template match="class">
<table 	style="text-align: justify; width: 95%; left; margin-left: auto; margin-right: auto;"
	border="0" cellpadding="10" cellspacing="2">
 <tbody>
  <tr>
   <td style="color: white; background-color: rgb(30, 130, 30);">
    <a href="{ID}.html" style="color: white; text-decoration: none;"><xsl:value-of select="name" /><small> (<xsl:value-of select="ID" />)</small></a>
   </td>
  </tr>
  <xsl:if test="requires/text">
   <tr>
    <td><xsl:value-of select="requires/text" /><br />
    <xsl:for-each select="requires">
     <xsl:if test="count(principle)+count(class)&gt;0">
      <br/>
      Requires:<br/>
      <xsl:apply-templates />
     </xsl:if>
    </xsl:for-each>
    <xsl:for-each select="partOf">
     <xsl:if test="count(class)&gt;0">
      <br/>
      Part of:<br/>
      <xsl:apply-templates />
     </xsl:if>
    </xsl:for-each>
    <xsl:for-each select="tags">
     <xsl:if test="count(tag)">
      <br/>
      Approved by:<br/>
      <xsl:apply-templates />
     </xsl:if>
    </xsl:for-each>
    </td>
   </tr>
  </xsl:if>
 </tbody>
</table>
</xsl:template>

<xsl:template match="tag">
<table  style="text-align: justify; width: 95%; left; margin-left: auto; margin-right: auto;"
        border="0" cellpadding="10" cellspacing="2">
 <tbody>
  <tr>
   <td style="color: white; background-color: rgb(130, 30, 30);">
    <a href="{ID}.html" style="color: white; text-decoration: none;"><xsl:value-of select="owner" /><small> (<xsl:value-of select="ID" />)</small></a>
   </td>
  </tr>
  <tr>
   <td><span style="font-weight: bold;"><xsl:value-of select="fingerprint" /></span><br /><br/>
   <xsl:value-of select="signature" />
   <xsl:if test="count(refersTo/principle)+count(refersTo/class)&gt;0">
    <br/><br/>
    Refers to:<br/>
    <xsl:apply-templates />
   </xsl:if>
   </td>
  </tr>
 </tbody>
</table> 
</xsl:template>

<xsl:template match="text()" />
</xsl:stylesheet>
