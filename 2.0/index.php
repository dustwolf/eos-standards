<?php
/*-------------------------------------------------------------------------

     Copyright 2010 Jure Sah

     This file is part of Principles and Classes database.

     Principles and Classes database is free software: you can redistribute 
     it and/or modify it under the terms of the GNU General Public License 
     as published by the Free Software Foundation, either version 3 of the 
     License, or (at your option) any later version.

     Please refer to the README file for additional information.

-------------------------------------------------------------------------*/

function xError($code,$text) {
 header($_SERVER["SERVER_PROTOCOL"]." ".$code." ".$text);
 echo "<h1>".$code."</h1>";
 echo "<h2>".$text."</h2>";
 die();
}

//Decode path to ID
$src=substr(rawurldecode($_SERVER["PATH_INFO"]),1);
$ext=strrchr($src,"."); if($ext=="") $ext=".";
if(substr($src,1,1)=='-' && substr($src,10,1)=='-') {
 $ID=substr($src,2,4)."-".substr($src,6,2)."-".substr($src,8,2)." "
    .substr($src,11,2).":".substr($src,13,2).":".substr($src,15,2);
 $ID=addslashes($ID);
} else { 
 //not found
 xError(404,"Not found");
}

if($ext==".") {
 header("Content-type: application/xml;charset=utf-8");
} else {
 if(!file_exists(substr($ext,1).".mime") || !file_exists(substr($ext,1).".xsl")) {
  xError(415,"Unsupported Media Type");
 }
 ob_start();
}

function cleanID($gID) {
 $gID=str_replace("-","",stripslashes($gID));
 $gID=str_replace(":","",$gID);
 $gID=str_replace(" ","-",$gID);
 return $gID;
}

function getID($result) {
 return cleanID(mysql_result($result,0,"ID"));
}

function tags($ID) {
 $results="";
 $tagged=mysql_query("SELECT ID,Owner,Fingerprint FROM netTags WHERE Signature LIKE '%".$ID."%' ORDER BY ID DESC");
 $rows=mysql_numrows($tagged);
 if($rows!=0) {
  $row=0;
  while($row<$rows) {
   $results=$results."
    <tag>
     <ID>3-".cleanID(mysql_result($tagged,$row,"ID"))."</ID>
     <owner>".stripslashes(mysql_result($tagged,$row,"Owner"))."</owner>
     <fingerprint>".stripslashes(mysql_result($tagged,$row,"Fingerprint"))."</fingerprint>
    </tag>";
   $row++;
  }
 }
 return $results;
}

function partOf($ID) {
 $results="";
 $partOf=mysql_query("SELECT ID,friendlyName FROM netClasses WHERE Requires LIKE '%".$ID."%' ORDER BY ID DESC");
 $rows=mysql_numrows($partOf);
 if($rows!=0) {
  $row=0;
  while($row<$rows) {
   $results=$results."
    <class>
     <ID>2-".cleanID(mysql_result($partOf,$row,"ID"))."</ID>
     <name>".stripslashes(mysql_result($partOf,$row,"friendlyName"))."</name>
    </class>";
   $row++;
  }
 }
 return $results;
}

function contains($text) {
 $results="";
 $length=strlen($text);
 $postition=0;
 while($position<$length) {
  if(substr($text,$position,2)=="1-" || substr($text,$position,2)=="2-" || substr($text,$position,2)=="3-") {
   $ID=substr($text,$position,17);
   if(substr($ID,10,1)=="-") {
    $sqlID=substr($ID,2,4)."-".substr($ID,6,2)."-".substr($ID,8,2)." "
          .substr($ID,11,2).":".substr($ID,13,2).":".substr($ID,15,2);
    $sqlID=mysql_real_escape_string($sqlID); 

    $labelText=$ID;
    $IDType=substr($ID,0,1);
    if($IDType=="1") {
     $labelFetch=mysql_query("SELECT ID,friendlyName FROM netPrinciples WHERE ID='".$sqlID."'");
     if(mysql_numrows($labelFetch)>0) {
      $labelText=stripslashes(mysql_result($labelFetch,0,"friendlyName"));
      $results=$results."<principle><ID>".$ID."</ID><name>".$labelText."</name></principle>";
     }
    } elseif($IDType=="2") {
     $labelFetch=mysql_query("SELECT ID,friendlyname FROM netClasses WHERE ID='".$sqlID."'");
     if(mysql_numrows($labelFetch)>0) {
      $labelText=stripslashes(mysql_result($labelFetch,0,"friendlyName")); 
      $results=$results."<class><ID>".$ID."</ID><name>".$labelText."</name></class>";
     }
    } elseif($IDType=="3") {
     $labelFetch=mysql_query("SELECT ID,Owner FROM netTags WHERE ID='".$sqlID."'");
     if(mysql_numrows($labelFetch)>0) {
      $labelText=stripslashes(mysql_result($labelFetch,0,"Owner"));
      $results=$results."<tag><ID>".$ID."</ID><owner>".$labelText."</owner></tag>";
     }
    }
    $position=$position+16; 
   }
  }
  $position++;
 }
 return $results;
}

?>
<standards file="<?php echo $src; ?>">
<?php
include '../credentials.php';
mysql_connect($hostname, $username, $password);
mysql_set_charset('utf8');
mysql_select_db("netPrinciples");

//Get first ID digit to determine type
$IDType = substr($src,0,1);

if($IDType=='2') {
 $class=mysql_query("SELECT * FROM netClasses WHERE ID='".$ID."'");
 if(mysql_numrows($class)==0) {
  xError(404,"Not Found");
 }

?>
 <class>
  <ID><?php $classID="2-".getID($class); echo $classID; ?></ID>
  <name><?php echo stripslashes(mysql_result($class,0,"friendlyName")); ?></name>
  <requires>
   <text><?php $requires=stripslashes(mysql_result($class,0,"Requires")); echo $requires; ?></text>
    <?php
     echo contains($requires);
    ?>
  </requires>
  <partOf><?php echo partOf($classID); ?></partOf>
  <tags><?php echo tags($classID); ?></tags>
 </class>
<?php

} elseif($IDType=='1') {
 $principle=mysql_query("SELECT * FROM netPrinciples WHERE ID='".$ID."'");
 if(mysql_numrows($principle)==0) {
  xError(404,"Not Found");
 }

?>
 <principle>
  <ID><?php $principleID="1-".getID($principle); echo $principleID; ?></ID>
  <name><?php echo stripslashes(mysql_result($principle,0,"friendlyName")); ?></name>
  <definition><?php echo stripslashes(mysql_result($principle,0,"Definition")); ?></definition>
  <partOf><?php echo partOf($principleID); ?></partOf> 
  <tags><?php echo tags($principleID); ?></tags>
 </principle>
   <?php
} elseif($IDType=='3') {
 $tag=mysql_query("SELECT * FROM netTags WHERE ID='".$ID."'");
 if(mysql_numrows($tag)==0) {
  xError(404,"Not Found");
 }
?>
 <tag>
  <ID><?php echo "3-".getID($tag); ?></ID>
  <owner><?php echo stripslashes(mysql_result($tag,0,"Owner"));?></owner>
  <fingerprint><?php echo stripslashes(mysql_result($tag,0,"Fingerprint"));?></fingerprint>
  <signature><?php $signature = stripslashes(mysql_result($tag,0,"Signature")); echo $signature; ?></signature>
  <refersTo>
   <?php
    echo contains($signature);
   ?>
  </refersTo>
 </tag>
<?php } ?>
</standards>
<?php
mysql_close();

if($ext!=".") {
 $internalXML=ob_get_contents();
 ob_end_clean();
 $file=substr($ext,1).".mime";
 $fn = fopen($file,"r"); 
  $mime=trim(fread($fn, filesize($file)));
 fclose($fn);
 header("Content-type: ".$mime.";charset=utf-8");
 passthru("echo ".escapeshellarg($internalXML)." | xsltproc ".escapeshellarg(substr($ext,1).".xsl")." -");
 ob_end_clean();
}
?>
