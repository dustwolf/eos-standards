<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		version="1.0">

<xsl:output 	method="xml"
		encoding="UTF-8"
		indent="no" />

<!--

     Copyright 2010 Jure Sah

     This file is part of Principles and Classes database.

     Principles and Classes database is free software: you can redistribute 
     it and/or modify it under the terms of the GNU General Public License 
     as published by the Free Software Foundation, either version 3 of the 
     License, or (at your option) any later version.

     Please refer to the README file for additional information.

-->

<!-- copy all elements and their attributes-->
<xsl:template match="standards">
 <standardsWiki><data><xsl:apply-templates /></data></standardsWiki>
</xsl:template>

<xsl:template match="principle">
{| cellpadding="5" cellspacing="10" style="width: 95%; margin-left: auto; margin-right: auto;"
|-style="background:#81821E; color:white; a:link {color: white}"
|<xsl:value-of select="name" /> ([http://standards.ctrl-alt-del.si/2.0/index.php/<xsl:value-of select="ID" />.html <xsl:value-of select="ID" />])
<xsl:if test="definition">
|-
|<xsl:value-of select="definition" />
</xsl:if>
<xsl:for-each select="partOf">
<xsl:if test="count(class)&gt;0">
|-
|Part of:
|-
|<xsl:apply-templates/>
</xsl:if>
</xsl:for-each>
<xsl:for-each select="tags">
<xsl:if test="count(tag)&gt;0">
|-
|Approved by:
|-
|<xsl:apply-templates/>
</xsl:if>
</xsl:for-each>
|}
</xsl:template>

<xsl:template match="class">
{| cellpadding="5" cellspacing="10" style="width: 95%; margin-left: auto; margin-right: auto;"
|-style="background:#1E821E; color:white; a:link {color: white}"
|<xsl:value-of select="name" /> ([http://standards.ctrl-alt-del.si/2.0/index.php/<xsl:value-of select="ID" />.html <xsl:value-of select="ID" />])
<xsl:if test="requires/text">
|-
|<xsl:value-of select="requires/text" />
</xsl:if>
<xsl:for-each select="requires">
<xsl:if test="count(principle)+count(class)&gt;0">
|-
|Requires:
|-
|<xsl:apply-templates/>
</xsl:if>
</xsl:for-each>
<xsl:for-each select="partOf">
<xsl:if test="count(class)&gt;0">
|-
|Part of:
|-
|<xsl:apply-templates/>
</xsl:if>
</xsl:for-each>
<xsl:for-each select="tags">
<xsl:if test="count(tag)&gt;0">
|-
|Approved by:
|-
|<xsl:apply-templates/>
</xsl:if>
</xsl:for-each>
|}
</xsl:template>

<xsl:template match="tag">
{| cellpadding="5" cellspacing="10" style="width: 95%; margin-left: auto; margin-right: auto;"
|-style="background:#811E1E; color:white; a:link {color: white}"
|<xsl:value-of select="owner" /> ([http://standards.ctrl-alt-del.si/2.0/index.php/<xsl:value-of select="ID" />.html <xsl:value-of select="ID" />])
|-
|'''<xsl:value-of select="fingerprint" />'''
<xsl:if test="count(refersTo/principle)+count(refersTo/class)&gt;0">
|-
|Refers to:
|-
|<xsl:apply-templates/>
</xsl:if>
|}
</xsl:template>

<xsl:template match="text()" />

</xsl:stylesheet>
