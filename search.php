<?php
/*-------------------------------------------------------------------------

     Copyright 2008 Jure Sah

     This file is part of Principles and Classes database.

     Principles and Classes database is free software: you can redistribute 
     it and/or modify it under the terms of the GNU General Public License 
     as published by the Free Software Foundation, either version 3 of the 
     License, or (at your option) any later version.

     Please refer to the README file for additional information.

-------------------------------------------------------------------------*/
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php 
include "linkify.php";
function display($table,$prefix) {
 $pvrstice=mysql_numrows($table);
 if($pvrstice!=0) {
  $pvrstica=0;
  while($pvrstica<$pvrstice) {
   $pgID=stripslashes(mysql_result($table,$pvrstica,"ID"));
   $pgID=str_replace("-","",$pgID);
   $pgID=str_replace(":","",$pgID);
   $pgID=str_replace(" ","-",$pgID);
   $pgID=linkify($prefix.$pgID,0);
   $pfriendlyName=stripslashes(mysql_result($table,$pvrstica,"friendlyName"));
   echo '<br/>'.$pgID.' '.$pfriendlyName;
   $pvrstica++;
  }
  echo "<br/>";
 }
}

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta content="text/html; charset=UTF-8" http-equiv="content-type" />
  <title>Principles and Classes - Search</title>
</head>
<body>

<table style="text-align: left; width: 100%;" border="0"
cellpadding="20" cellspacing="10">
<tbody>
<tr>
<td style="vertical-align: top; 
	 width: 120px; 
	 background-color: rgb(71, 117, 255);">
<?php include "menu.php"; ?>
</td>
<td style="vertical-align: top; text-align: justify;">

<h1>Principles and Classes - Search</h1>

Please note that this database search is not the same as Google search, it will not correct your spelling and guess what you meant; but it is still good enough if you know what you're looking for and are simply not that good with ID numbers.<br/><br/>
<form method="post" action="<?php echo $PHP_SELF;?>">
 <input name="search" type="text" value="<?php echo $_POST["search"]; ?>"/>&nbsp;
 <button name="submit" type="submit">Search</button>
</form>

<?php
if ($_POST) {
include 'credentials.php';
mysql_connect($hostname, $username, $password);
mysql_set_charset('utf8');

 mysql_select_db("netPrinciples");
 
 display(mysql_query("SELECT ID,friendlyName FROM netClasses 
                      WHERE friendlyName LIKE '%".$_POST["search"]."%'"),"2-");
 display(mysql_query("SELECT ID,friendlyName FROM netPrinciples 
                      WHERE friendlyName LIKE '%".$_POST["search"]."%'"),"1-");
 display(mysql_query("SELECT ID,friendlyName FROM netClasses 
                      WHERE Requires LIKE '%".$_POST["search"]."%'"),"2-");
 display(mysql_query("SELECT ID,friendlyName FROM netPrinciples
                      WHERE Definition LIKE '%".$_POST["search"]."%'"),"1-");

mysql_close();
} 
?>
      </td>
    </tr>
  </tbody>
</table>

</body>
</html>

