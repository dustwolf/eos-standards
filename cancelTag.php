<?php
/*-------------------------------------------------------------------------

     Copyright 2008 Jure Sah

     This file is part of Principles and Classes database.

     Principles and Classes database is free software: you can redistribute 
     it and/or modify it under the terms of the GNU General Public License 
     as published by the Free Software Foundation, either version 3 of the 
     License, or (at your option) any later version.

     Please refer to the README file for additional information.

-------------------------------------------------------------------------*/
require_once 'Crypt/GPG.php';

$text=$_POST["definition"];

$word1='----BEGIN PGP SIGNED MESSAGE-----';
$word2='-----BEGIN PGP SIGNATURE-----';
if(strpos($text,$word1)!=1 || strpos($text,$word2)==False) $text="";

if($_POST && $_POST["captcha"]==$_SERVER['REMOTE_ADDR'] && strlen($text)>0) {

$gpg = new Crypt_GPG();

try {
$details = $gpg->verify($_POST["definition"]);
} catch(Exception $e) {
 die("Invalid signature! Click back in your browser and verify that the data you entered is valid. You should not alter the content after you signed it. Also, make sure that your public key is available on public key servers. If you are sure everything is okay on your end, contact the administrator of this database for assistance.");
}

if ($details[0]->isValid()) {
 $Fingerprint = $details[0]->getKeyFingerprint();
 $Owner = $details[0]->getUserId()->getName();
} else {
 die("Invalid signature! Click back in your browser and verify that the data you entered is valid. You should not alter the content after you signed it. Also, make sure that your public key is available on public key servers. If you are sure everything is okay on your end, contact the administrator of this database for assistance.");
}

include 'credentials.php';
mysql_connect($hostname, $username, $password);
mysql_set_charset('utf8');

 mysql_select_db("netPrinciples");

$l=strlen($text);
$p=0;
while($p<$l) {
 if(substr($text,$p,2)=="3-") {
  $src=substr($text,$p,17);
  if(substr($src,1,1)=='-' && substr($src,10,1)=='-') {
   $ID=substr($src,2,4)."-".substr($src,6,2)."-".substr($src,8,2)." "
      .substr($src,11,2).":".substr($src,13,2).":".substr($src,15,2);
   $ID=addslashes($ID);
   $FFetch=mysql_query("SELECT ID,Fingerprint FROM netTags WHERE ID='".$ID."'");
   if(mysql_numrows($FFetch)>0) {
    $FText=stripslashes(mysql_result($FFetch,0,"Fingerprint"));
    if($FText==$Fingerprint) { 
     mysql_query("DELETE FROM netTags WHERE ID='".$ID."'") or die(mysql_error());
    } else {
     die("You have attempted to cancel a Tag that is not your own.");
    }
   }
   $p=$p+16;
  } else {
   $p++;
  }
 } else {
  if(substr($text,$p,29)=="-----BEGIN PGP SIGNATURE-----") $p=$l;
  $p++;
 }
}

 header('Location: http://standards.ctrl-alt-del.si/index.php');

mysql_close();
} else {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta content="text/html; charset=UTF-8" http-equiv="content-type" />
  <title>Principles and Classes - Cancel Tag</title>
</head>
<body>

<table style="text-align: left; width: 100%;" border="0"
cellpadding="20" cellspacing="10">
<tbody>
<tr>
<td style="vertical-align: top; 
         width: 120px; 
         background-color: rgb(71, 117, 255);">
<?php include "menu.php"; ?>
</td>
<td style="vertical-align: top;">

<h1>Principles and Classes - Cancel Tag</h1>

Here you cancel an existing Tag. You can only cancel your own Tags. Sign the ID of the Tag(s) you are canceling using your PGP key.<br/>
<br/>
Make sure there is no heading or trailing data above or below the signature or your input will not be accepted.<br/>
<br/>
When you have submitted a valid entry, you will be automatically redirected to the main page.<br/>
<br/>
<form method="post" action="<?php echo $PHP_SELF;?>">
Signed content:<br/>
<textarea rows="10" cols="60" name="definition">
 <?php 
  if($_POST["definition"]!="") {
   echo $_POST["definition"];
  } else {
   echo substr(rawurldecode($_SERVER["PATH_INFO"]),1);
  }
 ?>
</textarea><br/><br/>
Also enter what you see here:<br/>
<img src="http://standards.ctrl-alt-del.si/captcha.php"><br/>
<input name="captcha" type="text" value="<?php echo $_POST["captcha"];?>" /><br/><br/>
<input type="submit" value="Publish">

</form>


      </td>
    </tr>
  </tbody>
</table>

</body>
</html>
<?php } ?>
