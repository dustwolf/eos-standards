<?php
/*-------------------------------------------------------------------------

     Copyright 2008 Jure Sah

     This file is part of Principles and Classes database.

     Principles and Classes database is free software: you can redistribute 
     it and/or modify it under the terms of the GNU General Public License 
     as published by the Free Software Foundation, either version 3 of the 
     License, or (at your option) any later version.

     Please refer to the README file for additional information.

-------------------------------------------------------------------------*/
//RSS MIME type
header("Content-type: application/rss+xml;charset=utf-8");

 //Connect to DB
 include 'credentials.php';
 mysql_connect($hostname, $username, $password);
 mysql_set_charset('utf8');
 mysql_select_db("netPrinciples");

//Header
echo '<?xml version="1.0" encoding="UTF-8" standalone="no"?>';
//Get parameter
$line=htmlentities(substr(rawurldecode($_SERVER["PATH_INFO"]),1),ENT_QUOTES,'UTF-8');
//Find this fingerprint
$tabela=mysql_query("SELECT Signature,Owner FROM netTags WHERE Fingerprint='".$line."'");
//If none found, return blank feed
if(mysql_numrows($tabela)==0) die("<rss/>");
?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
 <channel>
  <title>Principles and Classes tagged entries feed</title>
  <link>
   http://standards.ctrl-alt-del.si/rsstagged.php<?php echo $_SERVER["PATH_INFO"]; ?>
  </link>
  <atom:link href="http://standards.ctrl-alt-del.si/rsstagged.php<?php echo $_SERVER["PATH_INFO"]; ?>" rel="self" type="application/rss+xml" />
  <description>Principles and Classes tagged by
   <?php echo stripslashes(mysql_result($tabela,0,"Owner")); ?>
  </description>

<?php
//loop trough all matching Tags
$pp=0; $ll=mysql_numrows($tabela);
while($pp<$ll) {
 //loop trough Signature area of each Tag
 $text=stripslashes(mysql_result($tabela,$pp,"Signature"));
 $p=0; $l=strlen($text);
 while ($p<$l) {
  //look for IDs
  $src=substr($text,$p,17);
  if(substr($src,1,1)=='-' && substr($src,10,1)=='-') {
   $ID=substr($src,2,4)."-".substr($src,6,2)."-".substr($src,8,2)." "
       .substr($src,11,2).":".substr($src,13,2).":".substr($src,15,2);
   $ID=mysql_real_escape_string($ID);
   
   echo '<item>';
   echo '<pubDate>'.date(DATE_RFC822,mktime(substr($src,11,2),substr($src,13,2),substr($src,15,2),substr($src,6,2),substr($src,8,2),substr($src,2,4))).'</pubDate>'; 
 
   if(substr($src,0,1)=="1") {
    $stabela=mysql_query("SELECT * FROM netPrinciples WHERE ID='".$ID."'");
    if(mysql_numrows($stabela)>0) {
     $friendlyName=stripslashes(mysql_result($stabela,0,"friendlyName"));
     $Definition=stripslashes(mysql_result($stabela,0,"Definition"));
     echo '<title>'.$friendlyName.'</title>'; 
     echo '<description>'.$Definition.'</description>'; 
    } else {
     echo '<title>'.htmlentities($src,ENT_QUOTES,'UTF-8').'</title>';
    }
   }
   
   if(substr($src,0,1)=="2") {
    $stabela=mysql_query("SELECT * FROM netClasses WHERE ID='".$ID."'");
    if(mysql_numrows($stabela)>0) {
     $friendlyName=stripslashes(mysql_result($stabela,0,"friendlyName"));
     $D=stripslashes(mysql_result($stabela,0,"Requires"));
     echo '<title>'.$friendlyName.'</title>';
     echo '<description>'.$D.'</description>';
    } else {
     echo '<title>'.htmlentities($src,ENT_QUOTES,'UTF-8').'</title>';
    }
   }
  
   echo '<guid>http://standards.ctrl-alt-del.si/index.php/'.htmlentities($src,ENT_QUOTES,'UTF-8').'</guid>';
   echo '<link>http://standards.ctrl-alt-del.si/index.php/'.htmlentities($src,ENT_QUOTES,'UTF-8').'</link>';
   echo '</item>';
   $p=$p+17;
  } else {
   if(substr($text,$p,29)=="-----BEGIN PGP SIGNATURE-----") $p=$l;
   $p++;
  }
 }
 $pp++;
}
mysql_close();
?>
 </channel>
</rss>
