<?php
/*-------------------------------------------------------------------------

     Copyright 2008 Jure Sah

     This file is part of Principles and Classes database.

     Principles and Classes database is free software: you can redistribute 
     it and/or modify it under the terms of the GNU General Public License 
     as published by the Free Software Foundation, either version 3 of the 
     License, or (at your option) any later version.

     Please refer to the README file for additional information.

-------------------------------------------------------------------------*/
$src=substr(rawurldecode($_SERVER["PATH_INFO"]),1);
if(substr($src,1,1)=='-' && substr($src,10,1)=='-' && strlen($src)==17) {
 $ID=substr($src,2,4)."-".substr($src,6,2)."-".substr($src,8,2)." "
    .substr($src,11,2).":".substr($src,13,2).":".substr($src,15,2);
 $ID=addslashes($ID);
} elseif($src=="debug") {
 $ID="debug";
} else { 
 $ID=""; 
 header("Location: /about.php");
 die();
}
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
include "linkify.php";
include "Tagged.php"
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta content="text/html; charset=UTF-8" http-equiv="content-type" />
  <link rel="doap.xml" href="http://usefulinc.com/ns/doap#" />
<?php
 if($ID=="") {
  echo '<title>Principles and Classes - index</title>';
 } else {
  echo '<title>Principles and Classes - '.$src.'</title>';
 }
?>
</head>
<body>

<table style="text-align: left; width: 100%;" border="0"
       cellpadding="20" cellspacing="10">
  <tbody>
    <tr>
      <td style="vertical-align: top; 
                 width: 120px; 
                 background-color: rgb(71, 117, 255);">
       <?php include "menu.php"; ?>
      </td>
      <td style="vertical-align: top;">

<h1>Principles and Classes - view</h1>


<?php
include 'credentials.php';
mysql_connect($hostname, $username, $password);
mysql_set_charset('utf8');

 mysql_select_db("netPrinciples");
?>
<table style="text-align: left; width: 100%;" border="0"
       cellpadding="2" cellspacing="2">
  <tbody>

<?php
 if($ID=="debug") {
  $tabela=mysql_query("SELECT * FROM netClasses ORDER BY ID DESC");
  $vrstice=mysql_numrows($tabela);
 } else {
  if(substr($src,0,1)=='2') {
   $tabela=mysql_query("SELECT * FROM netClasses WHERE ID='".$ID."'");
   $vrstice=mysql_numrows($tabela);
  } else { $vrstice=0; }
 }
 if($vrstice!=0) {
  $vrstica=0;
  while($vrstica < $vrstice) {
   $gID=stripslashes(mysql_result($tabela,$vrstica,"ID"));
   $gID=str_replace("-","",$gID);
   $gID=str_replace(":","",$gID);
   $gID=str_replace(" ","-",$gID);
   $mID="2-".$gID;
   $gID=linkify($mID,0);
   $friendlyName=stripslashes(mysql_result($tabela,$vrstica,"friendlyName"));
   $Definition=stripslashes(mysql_result($tabela,$vrstica,"Requires"));
   echo
    '<tr><td style="width: 1px;"><h3>'.$gID.'</h3></td><td>'.$friendlyName.
    ' <small><a href="http://standards.ctrl-alt-del.si/newTag.php/'.$mID.'">[Tag]</a></small></td></tr>
     <tr><td colspan="2"><div style="text-align: justify;">Requires: '.linkify($Definition,1).'</div>';
   Tagged($mID);
   echo '</td></tr>';
   $vrstica++; 
  }
 }

 if($ID=="debug") {
  $tabela=mysql_query("SELECT * FROM netPrinciples ORDER BY ID DESC");
  $vrstice=mysql_numrows($tabela);
 } else {
  if(substr($src,0,1)=='1') {
   $tabela=mysql_query("SELECT * FROM netPrinciples WHERE ID='".$ID."'");
   $vrstice=mysql_numrows($tabela);
  } else { $vrstice=0; }
 }
 if($vrstice!=0) {
  $vrstica=0;
  while($vrstica < $vrstice) {
   $gID=stripslashes(mysql_result($tabela,$vrstica,"ID"));
   $gID=str_replace("-","",$gID);
   $gID=str_replace(":","",$gID);
   $mID="1-".str_replace(" ","-",$gID);
   $gID=linkify($mID,0);
   $friendlyName=stripslashes(mysql_result($tabela,$vrstica,"friendlyName"));
   $Definition=stripslashes(mysql_result($tabela,$vrstica,"Definition"));
   echo
    '<tr><td style="width: 1px;"><h3>'.$gID.'</h3></td><td>'.$friendlyName.
    ' <small><a href="http://standards.ctrl-alt-del.si/newTag.php/'.$mID.'">[Tag]</a></small></td></tr>
     <tr><td colspan="2"><div style="text-align: justify;">'.linkify($Definition,1).'</div>
     <br/>';

   $partOf=mysql_query("SELECT ID,friendlyName FROM netClasses WHERE Requires LIKE '%".$mID."%'");
   $pvrstice=mysql_numrows($partOf);
   if($pvrstice!=0) {
    echo '<br/>Part of: ';
    $pvrstica=0;
    while($pvrstica<$pvrstice) {
     $pgID=stripslashes(mysql_result($partOf,$pvrstica,"ID"));
     $pgID=str_replace("-","",$pgID);
     $pgID=str_replace(":","",$pgID);
     $pgID=str_replace(" ","-",$pgID);
     //$pgID=linkify("2-".$pgID,0);
     $pfriendlyName=stripslashes(mysql_result($partOf,$pvrstica,"friendlyName"));
     //echo $pgID.' ('.$pfriendlyName.')';
     echo '<a href="http://standards.ctrl-alt-del.si/index.php/2-'.$pgID.'">'.$pfriendlyName.'</a>';
     if($pvrstica < $pvrstice-1) echo ', ';
     $pvrstica++;
    }
   }
   echo '<br/>';
   Tagged($mID);
   echo '</td></tr>';
   $vrstica++;
  }
 }

 if($ID=="debug") {
  $tabela=mysql_query("SELECT * FROM netTags ORDER BY ID DESC");
  $vrstice=mysql_numrows($tabela);
 } else {
  if(substr($src,0,1)=='3') {
   $tabela=mysql_query("SELECT * FROM netTags WHERE ID='".$ID."'");
   $vrstice=mysql_numrows($tabela);
  } else { $vrstice=0; }
 }
 if($vrstice!=0) {
  $vrstica=0;
  while($vrstica < $vrstice) {
   $gID=stripslashes(mysql_result($tabela,$vrstica,"ID"));
   $gID=str_replace("-","",$gID);
   $gID=str_replace(":","",$gID);
   $gID=str_replace(" ","-",$gID);
   $mID="3-".$gID;
   $gID=linkify($mID,0);
   $friendlyName=stripslashes(mysql_result($tabela,$vrstica,"Owner"));
   $Fingerprint=stripslashes(mysql_result($tabela,$vrstica,"Fingerprint"));
   $Definition=stripslashes(mysql_result($tabela,$vrstica,"Signature"));
   $Definition=str_replace(array("\r\n", "\n", "\r"), "<br/>", $Definition);
   echo
    '<tr><td style="width: 1px;"><h3>'.$gID.'</h3></td><td>'.$friendlyName.'<br/>'.
    ' <a href="http://standards.ctrl-alt-del.si/rsstagged.php/'.$Fingerprint.'">'.$Fingerprint.'</a>'.
    ' <small><a href="http://standards.ctrl-alt-del.si/cancelTag.php/'.$mID.'">[Cancel]</a></small></td></tr>
     <tr><td colspan="2"><div style="text-align: justify;">'.linkify($Definition,0).'</div><br/></td></tr>';
   $vrstica++;
  }
 }

?>
<p><a href="../../2.0/index.php/<?php echo $src ;?>.html">View in new interface.</a></p>
<p><a href="../../2.0/recursive.php/<?php echo $src ;?>.html">View complete text in new interface.</a></p>


  </tbody>
</table>
<?php
mysql_close();
?>

      </td>
    </tr>
  </tbody>
</table>

</body>
</html>
